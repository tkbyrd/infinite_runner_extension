Mechanics:

-Keys to collect.
-3 keys necessary in order to pass gates (after 3, able to pass through the wall, though it uses up 3, otherwise the wall is unpassable and the player dies)
-Pitfall traps that kill the player at the bottom.

Known Bugs / Issues
-Keys don't spawn in pre-positioned areas (spawn in those positions but also in the positions of the rocks, need to spawn ONLY in their respective places.)
-Keys don't vanish when gotten, don't add to the score, don't react to the player whatsoever.
-Unsure how to implement gates that, if the player has more than 3 keys, subtracts and lets the player continue, but otherwise, kills the player.
-Unsure how to implement gates that appear after X amount of platforms have already appeared.
-Unsure how to make the spawn rate of key sections much lower than other sections. 
-Unsure how to create a pitfall where past a certain point the player dies.
